# References

Reference Pages are useful for quick lookups of info that you just shouldn't have to remember. If you have forgotten how sub-APIs connect to the Duke API system, or what the individual parameters in .gitlab-ci.yml do, a reference page is probably what you want.

TODO