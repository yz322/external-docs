# What does it do?

The ePrint API does everything but print. It gives detailed information about printers, printer locations, printer statuses, and printer owners. Perfect for building an app that is aware of all of the ePrinters available around your users.

## /eprint/printers

Requests to this node take no parameters (other than access_key), and return an array of printer objects of the form below. Each printer has a unique id associated with it to be used in the other nodes of the API. type describes the type of printer "B&W" or "Color". name, brand, and model, are pretty self-explanatory. severity describes the severity of any problems with the printer. It is a string containing an integer ranging from "0" to "2". "0" indicates no problem. "1" indicates a warning. "2" indicates that the printer is not working. created_at and updated_at are also self-explanatory. statuses contains an array of status objects (or none), which each have a name,severity, and created_at. The severity of the printer matches the highest severity of its statuses. Each printer also has an associated site (with a unique site_id to be used in further API calls). The site describes the placement of the printer.

```javascript
 {
    "id": "85",
    "type": "B&W",
    "name": "SocSci Grad Lounge",
    "brand": "Dell",
    "model": "5330dn Mono Laser",
    "severity": "2",
    "created_at": "2012-03-21 12:24:36",
    "updated_at": "2014-07-01 16:02:01",
    "statuses": [
      {
        "name": "Offline",
        "severity": "2",
        "created_at": "2014-07-01 16:00:36"
      }
    ],
    "site_id": "4",
    "site": "SocSci Grad Lounge"
  }
```

## /eprint/printers/id

This node takes an id parameter as well as an access_key. The id can be acquired from the previous node. It returns the exact same array structure as /eprint/printers, but with only one object in the array (the one that you specified with id).

## /eprint/departments

This node takes only the access_key parameter, and returns an array of "department" objects. These department objects describe the various departments that might be in charge of certain ePrint stations. Each department has an id (which is different than a printer id. They also have a name, description, email, and phone, holding the information you would expect them to as strings. The url field holds the url representing the ePrint-management division of that department. created_at and updated_at appear to be largely arbitrary and shouldn't be expected to hold any important information.

```javascript
{
    "id": "13",
    "name": "Trinity",
    "description": "Any printers maintained by Trinity Technical Services are expected to be used primarily by the department having requested printers, and their staff, faculty and graduate students. To report issues with a specific device, please send an email to tts-av@duke.edu, with LOCATION of problematic printer.",
    "email": "tts-av@duke.edu",
    "phone": "(919) 660-3088",
    "url": "http://trinity.duke.edu/technology/classroom-help",
    "created_at": "2013-09-25 14:12:14",
    "updated_at": "2013-10-07 12:15:47"
  }
```

## /eprint/departments/id

This node takes an id (department id, not printer id) parameter as well as the access_key. It returns an array containing only the department object specified by the id given.

## /eprint/sites

This is a cool part of the ePrint API. It takes no parameters, but returns an array of "site" objects. These objects have an id that is referenced by the site_id in printer objects. This way you can link printers and sites. These site objects also have a department_id field which corresponds to the id of whichever department is responsible for the printer(s) at that site (whose name is described as a string by department). name, campus, building, and room should explain themselves. directions is a really cool feature, which will often (but not always) give written, plain english directions to the printer from inside the building. Very helpful if it's a hard to find printer or a big building. latitude and longitude are the coordinates of the printer itself as strings, and are also very useful. created_at and updated_at should be ignored again. retired describes whether a printer is still in service ("0" for working, "1" for retired). computers is false if there are no computers at that site, and holds an integer (the number of computers) if there are computers at the site. printers is an integer describing the number of printers at the site.

```javascript
{
    "id": "153",
    "name": "2106 Campus Drive",
    "department_id": "4",
    "campus": "Central",
    "building": "Grad House",
    "room": "",
    "directions": null,
    "latitude": "36.000407",
    "longitude": "-78.930985",
    "retired": "0",
    "created_at": "2013-10-18 11:40:13",
    "updated_at": "2013-10-18 11:40:13",
    "department": "Economics",
    "computers": false,
    "printers": 1
  }
```

## /eprint/sites/id

As you might expect by now, this takes an id parameter, and spits out an array containing only the site object that the id corresponds to.

# How do I use it?

As with all of the duke APIs, all you have to do is send an HTTP GET request to (aka pull down the webpage of) the url listed, appending "?access_token=YOURTOKEN". For this particular API, caching the big lists of printers, sites, and departments makes sense. Pull that information down once, and you won't need to connect to the network again for a while. This will speed up your apps. Only pull printers again when you want to check if any printers have changed statuses.

Using the id parameters as references within the nodes is really helpful, and allows you to have a lot of information about each printer, or each building, etc. Again though, I would not use the /id nodes, and would rather compare id's after you pull the full list, using the data cached on your user's device. This way, you won't incur the latency of pulling the data down each time you want to look up something by id.