# What is this about?

The Curriculum API is designed to allow external applications to look through all of the courses offered at Duke. Significant information about subjects, course offerings, synopses, etc. can be pulled down from this behemoth of an API.

# API nodes

There are several nodes available to use, all viewable on the documentation page of streamer. Normally I would do an in-depth description of each, but there is just so much data that this page would become very, very long. There is more value in viewing the inputs and outputs at the streamer doc page anyway. Suffice it to say that you can get a list of subjects, a list of courses in each subject, a list of classes in each subject, synopses of each class... etc. Yes, you CAN build a new duke schedulator.

# How do I use it?

As with all of the duke APIs, all you have to do is send an HTTP GET request to (aka pull down the webpage of) the url listed, appending "?access_token=YOURTOKEN", as well as "&PARAMETER=VALUE" as necessary. For this particular API, you honestly have to get to know it pretty well before you can start using it. Its directories are organized in a bit of an unexpected way, and I'd recommend making quite a few API calls on the streamer docs page before actually using it. It looks like much of it is converted from XML format to JSON, which is why it's so convoluted in places. Don't give up though, and try using [this JSON viewer](http://jsonviewer.stack.hu/) to get a better mental picture of the data.