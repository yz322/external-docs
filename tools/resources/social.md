# What does it do?

The social API is pretty simple in its goals, and in its implementation. It just spits out a list of the most recent Duke official social media posts. That's it.

## /social/messages

There's only one node to this API, and it's /social/messages. It takes no parameters (other than your access_key), and outputs an array of objects of the form below. id is an id associated with whatever platform the post is from (Twitter, facebook, etc). The login field is the screenname of the account. title is the title of the post, and body is the body. Be aware that these may contain syntax from whatever platform they come from. Twitter posts might have hashtags and the like, while facebook posts can contain html. date_posted is an [ISO 8601](http://en.wikipedia.org/wiki/ISO_8601)-formatted string containing the date and time of the post. source is a string holding "Twitter" or "Facebook", whichever platform the post is on. source_url is the url for the post.

```javascript
{
    "id": 484054589336211460,
    "login": "DukeU",
    "title": "US isn't prepared for robot/drone warefare, explains professor @Missy_Cummings at #AspenIdeas: http://t.co/y1vO7RZrBS",
    "body": "US isn't prepared for robot/drone warefare, explains professor @Missy_Cummings at #AspenIdeas: http://t.co/y1vO7RZrBS",
    "date_posted": "2014-07-01T19:23:02+00:00",
    "source": "Twitter",
    "source_url": "https://twitter.com/DukeU/status/484054589336211457"
}
```

# How do I use it?

As with all of the duke APIs, all you have to do is send an HTTP GET request to (aka pull down the webpage of) the url listed, appending "?access_token=YOURTOKEN". For this particular API, there isn't a whole lot of customization you can do. It's more or less a static response you're pulling down. I'd caution against requesting too rapidly, as this will slow down your app, and it's not like Duke is posting updates every minute.