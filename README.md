# Welcome Innovators!

[![build status](http://gitlab.oit.duke.edu/colab/external-docs/badges/master/build.svg)](http://gitlab.oit.duke.edu/colab/external-docs/commits/master) <--(Learn how this whole site is automatically tested, built, and updated in our [CI Tutorial](guides/ci.md) :D) 


These documents are here to help guide you through anything we can think of that you might want to do, hardware or software. We place a special focus on using the tools that the Co-Lab has built for you to use at Duke. We've got some cool stuff available, and we want to make sure that everyone gets the chance to play with it!



-----------------------

## Contributions

Fork, edit, and push changes to these documents at [Gitlab](https://gitlab.oit.duke.edu/colab/external-docs). We love everyone who helps to expand this, and thanks in advance for making Duke an even better place!
The following people have helped to make these documents (in order of first contribution):

 - Jason "Toolbox" Oettinger - [Gitlab](https://gitlab.oit.duke.edu/u/toolbox)
