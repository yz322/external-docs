# How to Control the Photon in Co-Lab using Photon API (Experiemental)

We are buliding a System to control the electronics in the Co-Lab Studio. Currently, we developed a system to allow users control the Sony TV in the Co-Lab. This tutorial will help you send command to the photon remotely. 

## Features Supported
Currently we only supports control the power of Sony TV. If you want us to implement other features, please post to [our slack channel](https://dukecolab.slack.com/archives/arduino-photon). We will review your request and add more function!

## How to Use
To turn the TV on, type in your terminal:
```curl https://api.particle.io/v1/devices/2d0036001447343339383037/TVOn      
-d access_token=Not Shown      
-d "args= Not Shown"```

To turn the TV off, type in your terminal: 
```curl https://api.particle.io/v1/devices/2d0036001447343339383037/TVOff      
-d access_token=Not Shown     
-d "args= Not Shown"```

To prevent the abuse of this function, the access_token and args are not displayed here. If you want to use it, post in [our slack channel](https://dukecolab.slack.com/archives/arduino-photon) and request the key!


