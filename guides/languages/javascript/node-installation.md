# How to install Node.js 

This guide requires you to know:
 
 - [How to use Bash (the terminal)](languages/bash/intro.md)

---------------

_Windows and OSX can ignore this and just go download the node.js installer [here](https://nodejs.org/en/)_

One of the hardest parts of working in node is just figuring out the best way of installing it. There are many ways, each with advantages and disadvantages. We used to use nvm, which allows each user to install it separately, but now we recommend installing it from your linux package manager.
 
The upsides? Installing from apt allows every user to access node, without worrying about which packages are installed under which person. It also allows node to be updated whenever updates are run on the machine in general.

The downsides? you can only run one version of node at a time, and you must use sudo to install global npm packages.

Long story short, do [this](https://nodejs.org/en/download/package-manager/)