# Guides

The guides are here to help you get a feel for anything you might want to be able to do. It's hard to hear a textbook description of a webserver, and then go out and build one. It makes much more sense if you see someone else do it first.

## Software

Building, maintaining, designing, and understanding sofware is an important skill, often divorced from the more elegant theories of computer science. This is the good, bad, and ugly stuff that'll help you get whatever you want to build up and running!

 - [Language Guides](guides/languages.md): There's a bajillion languages. These will help you get off the ground in some of them (or at least help you pick the one for your project).
 - [Continuous Integration](guides/ci.md): This example will show you how you can automate the process of building, testing, and deploying your application, using this site as an example!
 - [Using Web APIs](guides/api.md): This example shows you that the word 'api' doesn't have to be scary, walking you through using the Duke API in Javascript.
 - [Using Git to manage your Code](guides/git.md): Using git is the best way to organize, back up, and collaborate on your code.
 - [Getting a server from Duke](guides/vm-manage.md): Duke provides a lot of development resources for you to learn with, including your own personal server. Learn how to set it up!
 - [Using ssh to control your servers](guides/git.md): ssh is the standard way of logging into servers and other remote machines, and running stuff on them.
 
## Hardware

Getting started with hardware can seem really daunting, because it all seems so magical. These guides de-mystify a lot of it, and show you that hardware is super fun!